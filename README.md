# Pic music buzzer

A buzzer playing music powered by a PIC12F1822 microcontroller. A little I made for playing sounds from a PhD hat after a doctoral defense.

## Background

I tried to port the awesome [arduino songs project](https://github.com/robsoncouto/arduino-songs) onto a somewhat cheaper PIC-based microcontroller (PIC12F1822) using pulse-width modulation to drive the buzzer. The entire thing is powered by two CR2032 batteries. 

*Note*: I am still new to this, so this is probably not the way to implement everything. If you have ideas on how to improve things: Great!

![Breadboard mock-up](/Breadboard.jpg)*Breadboard mock-up*
![Final circuit board](/Circuitboard.jpeg)*Final circuit board soldered together*

