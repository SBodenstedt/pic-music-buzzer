/*
 * 
 *  Code for playing music with a PIC12F1822 with a buzzer
 * 
 *  Made by Sebastian Bodenstedt (sebastian@bodenstedt.eu)
 *  www.bodenstedt.eu
 *  https://gitlab.com/SBodenstedt/pic-music-buzzer
 *
 */

// PIC12F1822 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable (PWRT enabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = ON        // Internal/External Switchover (Internal/External Switchover mode is enabled)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is enabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config PLLEN = OFF      // PLL Enable (4x PLL disabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LVP = OFF        // Low-Voltage Programming Enable (High-voltage on MCLR/VPP must be used for programming)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>

// the songs are from the awesome arduino songs project: https://github.com/robsoncouto/arduino-songs
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define REST      0

const int tempo[] = {80, 120, 144, 200};
const int length[] = {8, 87,99,49};
const int melody_1[] = {

  // Star Trek Intro
  // Score available at https://musescore.com/user/10768291/scores/4594271
 
  NOTE_D4, -8, NOTE_G4, 16, NOTE_C5, -4, 
  NOTE_B4, 8, NOTE_G4, -16, NOTE_E4, -16, NOTE_A4, -16,
  NOTE_D5, 2,
};
const int melody_2[] = {

  //Star Warts
  NOTE_A4,-4, NOTE_A4,-4, NOTE_A4,16, NOTE_A4,16, NOTE_A4,16, NOTE_A4,16, NOTE_F4,8, REST,8,//8
  NOTE_A4,-4, NOTE_A4,-4, NOTE_A4,16, NOTE_A4,16, NOTE_A4,16, NOTE_A4,16, NOTE_F4,8, REST,8,//8
  NOTE_A4,4, NOTE_A4,4, NOTE_A4,4, NOTE_F4,-8, NOTE_C5,16,//6

  NOTE_A4,4, NOTE_F4,-8, NOTE_C5,16, NOTE_A4,2,//4
  NOTE_E5,4, NOTE_E5,4, NOTE_E5,4, NOTE_F5,-8, NOTE_C5,16,//5
  NOTE_A4,4, NOTE_F4,-8, NOTE_C5,16, NOTE_A4,2,//4
  
  NOTE_A5,4, NOTE_A4,-8, NOTE_A4,16, NOTE_A5,4, NOTE_GS5,-8, NOTE_G5,16, //6 
  NOTE_DS5,16, NOTE_D5,16, NOTE_DS5,8, REST,8, NOTE_A4,8, NOTE_DS5,4, NOTE_D5,-8, NOTE_CS5,16,//8

  NOTE_C5,16, NOTE_B4,16, NOTE_C5,16, REST,8, NOTE_F4,8, NOTE_GS4,4, NOTE_F4,-8, NOTE_A4,-16,//8
  NOTE_C5,4, NOTE_A4,-8, NOTE_C5,16, NOTE_E5,2,//4

  NOTE_A5,4, NOTE_A4,-8, NOTE_A4,16, NOTE_A5,4, NOTE_GS5,-8, NOTE_G5,16, //6
  NOTE_DS5,16, NOTE_D5,16, NOTE_DS5,8, REST,8, NOTE_A4,8, NOTE_DS5,4, NOTE_D5,-8, NOTE_CS5,16,//8

  NOTE_C5,16, NOTE_B4,16, NOTE_C5,16, REST,8, NOTE_F4,8, NOTE_GS4,4, NOTE_F4,-8, NOTE_A4,-16,//8
  NOTE_A4,4, NOTE_F4,-8, NOTE_C5,16, NOTE_A4,2,//4
};
const int melody_3[] = {
  //TETRIS

    NOTE_E5, 4,  NOTE_B4,8,  NOTE_C5,8,  NOTE_D5,4,  NOTE_C5,8,  NOTE_B4,8,
  NOTE_A4, 4,  NOTE_A4,8,  NOTE_C5,8,  NOTE_E5,4,  NOTE_D5,8,  NOTE_C5,8,
  NOTE_B4, -4,  NOTE_C5,8,  NOTE_D5,4,  NOTE_E5,4,
  NOTE_C5, 4,  NOTE_A4,4,  NOTE_A4,8,  NOTE_A4,4,  NOTE_B4,8,  NOTE_C5,8,//22

  NOTE_D5, -4,  NOTE_F5,8,  NOTE_A5,4,  NOTE_G5,8,  NOTE_F5,8,
  NOTE_E5, -4,  NOTE_C5,8,  NOTE_E5,4,  NOTE_D5,8,  NOTE_C5,8,
  NOTE_B4, 4,  NOTE_B4,8,  NOTE_C5,8,  NOTE_D5,4,  NOTE_E5,4,
  NOTE_C5, 4,  NOTE_A4,4,  NOTE_A4,4, REST, 4,//19

  NOTE_E5, 4,  NOTE_B4,8,  NOTE_C5,8,  NOTE_D5,4,  NOTE_C5,8,  NOTE_B4,8,
  NOTE_A4, 4,  NOTE_A4,8,  NOTE_C5,8,  NOTE_E5,4,  NOTE_D5,8,  NOTE_C5,8,
  NOTE_B4, -4,  NOTE_C5,8,  NOTE_D5,4,  NOTE_E5,4,
  NOTE_C5, 4,  NOTE_A4,4,  NOTE_A4,8,  NOTE_A4,4,  NOTE_B4,8,  NOTE_C5,8,//22

  NOTE_D5, -4,  NOTE_F5,8,  NOTE_A5,4,  NOTE_G5,8,  NOTE_F5,8,
  NOTE_E5, -4,  NOTE_C5,8,  NOTE_E5,4,  NOTE_D5,8,  NOTE_C5,8,
  NOTE_B4, 4,  NOTE_B4,8,  NOTE_C5,8,  NOTE_D5,4,  NOTE_E5,4,
  NOTE_C5, 4,  NOTE_A4,4,  NOTE_A4,4, REST, 4,//19
  

  NOTE_E5,2,  NOTE_C5,2,
  NOTE_D5,2,   NOTE_B4,2,
  NOTE_C5,2,   NOTE_A4,2,
  NOTE_GS4,2,  NOTE_B4,4,  REST,8, 
  NOTE_E5,2,   NOTE_C5,2,
  NOTE_D5,2,   NOTE_B4,2,
  NOTE_C5,4,   NOTE_E5,4,  NOTE_A5,2,
  NOTE_GS5,2,//17
};
const int melody_4[] = {
  NOTE_E5,8,    NOTE_E5,8,  REST,8,     NOTE_E5,8, REST,8, NOTE_C5,8, NOTE_E5,8, //1
  NOTE_G5,4,    REST,4,     NOTE_G4,8,  REST,4, 
  NOTE_C5,-4,   NOTE_G4,8,  REST,4,     NOTE_E4,-4, // 3
  NOTE_A4,4,    NOTE_B4,4,  NOTE_AS4,8, NOTE_A4,4,
  NOTE_G4,-8,   NOTE_E5,-8, NOTE_G5,-8, NOTE_A5,4, NOTE_F5,8, NOTE_G5,8,
  REST,8,       NOTE_E5,4,  NOTE_C5,8,  NOTE_D5,8, NOTE_B4,-4,
  NOTE_C5,-4,   NOTE_G4,8,  REST,4,     NOTE_E4,-4, // repeats from 3
  NOTE_A4,4,    NOTE_B4,4,  NOTE_AS4,8, NOTE_A4,4,
  NOTE_G4,-8,   NOTE_E5,-8, NOTE_G5,-8, NOTE_A5,4, NOTE_F5,8, NOTE_G5,8,
  REST,8,       NOTE_E5,4,  NOTE_C5,8,  NOTE_D5,8, NOTE_B4,-4,
  
};

#define TMR2PRESCALE 4 

// Will run internal oscillator and PLL to give 1 MIPS
// This definition allows __delay_ms() and __delay_us() to be used
// if desired.
#define _XTAL_FREQ 1000000UL

void delayN(unsigned long delay)
{
    unsigned int i;
    for(i=0; i<delay; i++) 
        //NOP();
        __delay_ms(1);
}

#define USE_LED
#ifdef USE_LED
    #define LED1 LATAbits.LATA1
    #define LED1_TRIS TRISAbits.TRISA1
#endif

void playTone(unsigned int freq);

int main()
{
    //
    // 1 MHz Internal Oscillator
    //
    OSCCONbits.IRCF = 0b1011;
    OSCCONbits.SPLLEN = 0; // NO SPL
    WDTCONbits.SWDTEN = 0;
    WDTCONbits.WDTPS = 0b10010; //No idea why this is necessary, watchdog should be turned off, but without this line it still resets every 2 seconds
    
    while (1)
    for (int j = 0;j < 4;j++)// loop through all 4 songs
    {
        // music playing adapted from the awesome arduino songs project: https://github.com/robsoncouto/arduino-songs
        int wholenote = (54000 * 4) / tempo[j]; //play song for 90% of the time
        int wholenote_break = (6000 * 4) / tempo[j]; //break for 10% of the time
        const int* melody;
        if (j == 0) 
          melody = melody_1;
        else if (j == 1) 
          melody = melody_2;
        else if (j == 2) 
          melody = melody_3;
        else 
          melody = melody_4;
        for (int i = 0; i < length[j];i++)
        {
            int tone = melody[2*i];
            int divider = melody[2*i+1];
            unsigned int duration = 0;
            unsigned int duration_break = 0;

            if (divider > 0) 
            {
                // regular note, just proceed
                duration = (wholenote) / divider;
                duration_break = (wholenote_break) / divider;
            } 
            else if (divider<  0) 
            {
                // dotted notes are represented with negative durations!!
                duration = (wholenote) / abs(divider);
                duration = (duration*3)/2; // increases the duration in half for dotted notes
                duration_break = (wholenote_break) / abs(divider);
                duration_break = (duration_break*3)/2; // increases the duration in half for dotted notes
            }

            playTone(tone);
            delayN(duration);
            playTone(REST);
            delayN(duration_break);
        }
    }
}

// From code supplied at http://www.microchip.com/forums/m951273.aspx
void playTone(unsigned int freq) //play tone using PWM
{
    unsigned char tmr2prescale = 4;
    if (freq == 0)
        CCP1CONbits.CCP1M = 0;
    else
    {
        PR2 = (_XTAL_FREQ/(freq*tmr2prescale*4)) - 1;
        // PWM mode: P1A, P1B active-high
        CCP1CONbits.CCP1M = 0b1100;

        unsigned int duty = (5*_XTAL_FREQ)/(freq*tmr2prescale*10); // 
        CCP1CONbits.DC1B0 = duty & 1; //Store the 1st bit
        CCP1CONbits.DC1B1 = duty & 2; //Store the 0th bit
        CCPR1L = duty>>2;// Store the remining 8 bit

        // PWM P1A Output is on RA2 (Pin 5 of the DIP-8)
        TRISAbits.TRISA2 = 0;
        APFCONbits.CCP1SEL = 0;
        PIR1bits.TMR2IF = 0;

        // Leave prescale, postscale = 1:1, turn on the timer
        T2CONbits.TMR2ON = 1;
    }

}